#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/usart.h>

#include "macro.h"
#include "systick.h"
#include "monitor.h"
#include "config.h"
#include "pwrmon-def.h"

#define USE_MEMCHECK 0

#if USE_MEMCHECK
#include "memtool.h"
#endif

monitor_def();

static inline void hardware_init(void) {
  /* Enable GPIO clock */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOD);
	
	rcc_periph_clock_enable(RCC_AFIO);
}

static inline void hardware_done(void) {
  /* Disable GPIO clock */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOD);
}

static inline void serial_init(void) {
  rcc_periph_clock_enable(RCC_USART1);

	/* UART pins */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO_USART1_RX);

  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                GPIO_USART1_TX);

  /* Setup UART parameters. */
	usart_set_baudrate(USART1, 9600);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
  usart_set_mode(USART1, USART_MODE_TX_RX);
  
	/* Finally enable the USART. */
	usart_enable(USART1);
}

static inline void tester_init(void) {
	//nvic_enable_irq(NVIC_EXTI15_10_IRQ);

	gpio_set_mode(GPIOD, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO10);
	
	exti_select_source(EXTI10, GPIOD);
	exti_set_trigger(EXTI10, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI10);
	
	gpio_set_mode(GPIOD, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO11);
}

/*
void exti15_10_isr(void) {
	exti_reset_request(EXTI10);
}
*/

static void over_voltage_on(void) {
	gpio_set(GPIOD, GPIO11);
	gpio_set(GPIOD, GPIO9);
	gpio_set_mode(GPIOD, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO9);
}

static void over_voltage_off(void) {
	gpio_set_mode(GPIOD, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO9);
	gpio_clear(GPIOD, GPIO9);
	gpio_clear(GPIOD, GPIO11);
}

static void over_current_on(void) {
	gpio_set(GPIOD, GPIO11);
	gpio_set(GPIOD, GPIO8);
	gpio_set_mode(GPIOD, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                GPIO8);
}

static void over_current_off(void) {
	gpio_set_mode(GPIOD, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO8);
	gpio_clear(GPIOD, GPIO8);
	gpio_clear(GPIOD, GPIO11);
}

static inline void init(void) {
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  
  hardware_init();
  systick_init();
  serial_init();
	tester_init();
}

#if 0
static inline void done(void) {
  systick_done();
  hardware_done();
}
#else
#define done()
#endif

#if USE_MEMCHECK
static mem_info_t mem_info;
#endif

static void exchange(const pwrmon_packet_t spkt, pwrmon_packet_t rpkt) {
	usart_send_blocking(USART1, spkt[0]);
	usart_send_blocking(USART1, spkt[1]);
	rpkt[0] = usart_recv_blocking(USART1);
	rpkt[1] = usart_recv_blocking(USART1);
}

static uint16_t max_voltage;
static uint16_t max_current;
static uint16_t voltage;
static uint16_t current;
static uint16_t over_voltage;
static uint16_t over_current;

int main(void) {
#if USE_MEMCHECK
  mem_prefill();
#endif
  
  init();
  monitor_init();
  
  for (;;) {
    monitor_wait();

		pwrmon_packet_t spkt, rpkt;

		pwrmon_mk_packet(spkt, pwrmon_set_max_voltage, 300);
		exchange(spkt, rpkt);

		pwrmon_mk_packet(spkt, pwrmon_set_max_current, 17);
		exchange(spkt, rpkt);

		pwrmon_mk_packet(spkt, pwrmon_get_voltage, 0);
		exchange(spkt, rpkt);
		voltage = pwrmon_get_value(rpkt);

		pwrmon_mk_packet(spkt, pwrmon_get_current, 0);
		exchange(spkt, rpkt);
		current = pwrmon_get_value(rpkt);

		pwrmon_mk_packet(spkt, pwrmon_get_max_voltage, 0);
		exchange(spkt, rpkt);
		max_voltage = pwrmon_get_value(rpkt);

		pwrmon_mk_packet(spkt, pwrmon_get_max_current, 0);
		exchange(spkt, rpkt);
		max_current = pwrmon_get_value(rpkt);
		
		monitor_wait();
		//
		over_voltage_on();
		monitor_wait();
		//
		pwrmon_mk_packet(spkt, pwrmon_get_voltage, 0);
		exchange(spkt, rpkt);
		over_voltage = pwrmon_get_value(rpkt);
		//
		monitor_wait();
		over_voltage_off();
		
		monitor_wait();
		//
		over_current_on();
		monitor_wait();
		//
		pwrmon_mk_packet(spkt, pwrmon_get_current, 0);
		exchange(spkt, rpkt);
		over_current = pwrmon_get_value(rpkt);
		//
		monitor_wait();
		over_current_off();
		
		monitor_wait();
		
#if USE_MEMCHECK
    mem_measure(&mem_info);
#endif
  }
  
  done();
  
  return 0;
}
