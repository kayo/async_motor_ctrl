IMG_P = bin/$(1).bin

define IMG_RULES
$$(eval $$(call BIN_RULES,$(1)))

$(1).IMG := $$(call IMG_P,$(1))
build: build.img.$(1)
build.img.$(1): $$($(1).IMG)
$$($(1).IMG): $$($(1).BIN)
	@echo TARGET $(1) IMAGE
	$(Q)$(OBJCOPY) -O binary $$< $$@

clean: clean.img.$(1)
clean.img.$(1):
	$(Q)rm -f $$($(1).IMG)

$$(eval $$(call FLASH_RULES,$(1)))
endef
