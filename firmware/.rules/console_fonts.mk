TARGET.LIBS += console_fonts
console_fonts.INHERIT := firmware
console_fonts.list ?= font_8x8 font_8x16
console_fonts.SRCS += $(patsubst %,$(SRCDIR)/%.psf.S,$(console_fonts.list))

BLOB_NAME = $(subst .,_,$(notdir $(1)))
$(SRCDIR)/%.psf.S:: $(SRCDIR)/%.psf
	@echo GEN SRC BLOB $(call BLOB_NAME,$*)
	$(Q)echo '  .section .rodata' > $@
	$(Q)echo '  .global $(call BLOB_NAME,$*)_start' >> $@
	$(Q)echo '$(call BLOB_NAME,$*)_start:' >> $@
	$(Q)echo '  .incbin "$<"' >> $@
	$(Q)echo '  .global $(call BLOB_NAME,$*)_end' >> $@
	$(Q)echo '$(call BLOB_NAME,$*)_end:' >> $@
