#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/dma.h>

#include "macro.h"
#include "config.h"
#include "pcd8544_def.h"
#include "pcd8544.h"

#define PCD8544_SPI_DEV _CAT2(SPI, _NTH0(pcd8544_spi))
#define PCD8544_SPI_RCC _CAT2(RCC_SPI, _NTH0(pcd8544_spi))
#define PCD8544_SPI_DIV _CAT2(SPI_CR1_BAUDRATE_FPCLK_DIV_, _NTH1(pcd8544_spi))

#ifdef pcd8544_dma
#define PCD8544_DMA
#define PCD8544_DMA_DEV _CAT2(DMA, _NTH0(pcd8544_dma))
#define PCD8544_DMA_CH _CAT2(DMA_CHANNEL, _NTH1(pcd8544_dma))
#define PCD8544_DMA_IRQ _CAT5(NVIC_DMA, _NTH0(pcd8544_dma), _CHANNEL, _NTH1(pcd8544_dma), _IRQ)
#define pcd8544_dma_isr _CAT5(dma, _NTH0(pcd8544_dma), _channel, _NTH1(pcd8544_dma), _isr)
#endif

#define PCD8544_SPI_REMAP _CAT3(spi, _NTH0(pcd8544_spi), _remap)

#define PCD8544_DI_PORT _CAT4(GPIO_BANK_SPI, _NTH0(pcd8544_spi), PCD8544_SPI_REMAP, _MOSI)
#define PCD8544_DI_PAD _CAT4(GPIO_SPI, _NTH0(pcd8544_spi), PCD8544_SPI_REMAP, _MOSI)
#define PCD8544_DI_AF _CAT2(GPIO, PCD8544_SPI_REMAP)
#define PCD8544_SCK_PORT _CAT4(GPIO_BANK_SPI, _NTH0(pcd8544_spi), PCD8544_SPI_REMAP, _SCK)
#define PCD8544_SCK_PAD _CAT4(GPIO_SPI, _NTH0(pcd8544_spi), PCD8544_SPI_REMAP, _SCK)
#define PCD8544_SCK_AF _CAT2(GPIO, PCD8544_SPI_REMAP)

#ifdef pcd8544_cs
#define PCD8544_CS_PORT _CAT2(GPIO, _NTH0(pcd8544_cs))
#define PCD8544_CS_PAD _CAT2(GPIO, _NTH1(pcd8544_cs))
#else
#define PCD8544_CS_PORT _CAT4(GPIO_BANK_SPI, _NTH0(pcd8544_spi), PCD8544_SPI_REMAP, _NSS)
#define PCD8544_CS_PAD _CAT4(GPIO_SPI, _NTH0(pcd8544_spi), PCD8544_SPI_REMAP, _NSS)
#endif

#define PCD8544_DC_PORT _CAT2(GPIO, _NTH0(pcd8544_dc))
#define PCD8544_DC_PAD _CAT2(GPIO, _NTH1(pcd8544_dc))
#define PCD8544_RES_PORT _CAT2(GPIO, _NTH0(pcd8544_res))
#define PCD8544_RES_PAD _CAT2(GPIO, _NTH1(pcd8544_res))

#ifdef pcd8544_bl_pwm
#define PCD8544_BL_PWM
#define PCD8544_BL_DEV _CAT2(TIM, _NTH0(pcd8544_bl_pwm))
#define PCD8544_BL_CH _CAT2(TIM_OC, _NTH1(pcd8544_bl_pwm))

#define PCD8544_BL_REMAP _CAT3(tim, _NTH0(pcd8544_bl_pwm), _remap)

#define PCD8544_BL_PORT _CAT5(GPIO_BANK_TIM, _NTH0(pcd8544_bl_pwm), PCD8544_BL_REMAP, _CH, _NTH1(pcd8544_bl_pwm))
#define PCD8544_BL_PAD _CAT5(GPIO_TIM, _NTH0(pcd8544_bl_pwm), PCD8544_BL_REMAP, _CH, _NTH1(pcd8544_bl_pwm))
#define PCD8544_BL_AF _CAT2(GPIO, PCD8544_BL_REMAP)
#endif /* pcd8544_bl_pwm */

#ifdef pcd8544_bl
#define PCD8544_BL_GPO
#define PCD8544_BL_PORT _CAT2(GPIO, _NTH0(pcd8544_bl))
#define PCD8544_BL_PAD _CAT2(GPIO, _NTH1(pcd8544_bl))
#endif /* pcd8544_bl */

#ifdef pcd8544_bl_on
#define PCD8544_BL_ON (pcd8544_bl_on)
#else
#define PCD8544_BL_ON 1
#endif

static inline void spi_wait_tx(uint32_t spi) {
  /* Wait to transmit last data */
  for (; !(SPI_SR(spi) & SPI_SR_TXE); );
}

static inline void spi_wait_busy(uint32_t spi) {
  /* Wait until not busy */
  for (; SPI_SR(spi) & SPI_SR_BSY; );
}

static inline void pcd8544_spi_enable(void) {
  gpio_clear(PCD8544_CS_PORT, PCD8544_CS_PAD);
  spi_enable(PCD8544_SPI_DEV);
}

static inline void pcd8544_spi_disable(void) {
  spi_wait_tx(PCD8544_SPI_DEV);
  spi_wait_busy(PCD8544_SPI_DEV);
  spi_disable(PCD8544_SPI_DEV);
  gpio_set(PCD8544_CS_PORT, PCD8544_CS_PAD);
}

uint8_t pcd8544_frame_buffer[pcd8544_frame_bytes];

static inline void pcd8544_set_command(void) {
  gpio_clear(PCD8544_DC_PORT, PCD8544_DC_PAD);
}

static inline void pcd8544_set_data(void) {
  gpio_set(PCD8544_DC_PORT, PCD8544_DC_PAD);
}

static inline void pcd8544_command(uint8_t command) {
  spi_send(PCD8544_SPI_DEV, command);
}

void pcd8544_init(void){
  rcc_periph_clock_enable(PCD8544_SPI_RCC);

  /* SPI bus outputs */
#ifdef STM32F1
  gpio_set_mode(PCD8544_DI_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PCD8544_DI_PAD);
  gpio_set_mode(PCD8544_SCK_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PCD8544_SCK_PAD);
  gpio_set_mode(PCD8544_CS_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_CS_PAD);
#else
  gpio_mode_setup(PCD8544_DI_PORT, GPIO_MODE_AF,
                  GPIO_PUPD_NONE, PCD8544_DI_PAD);
  gpio_set_output_options(PCD8544_DI_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_DI_PAD);
  gpio_set_af(PCD8544_DI_PORT, PCD8544_DI_AF, PCD8544_DI_PAD);

  gpio_mode_setup(PCD8544_SCK_PORT, GPIO_MODE_AF,
                  GPIO_PUPD_NONE, PCD8544_SCK_PAD);
  gpio_set_output_options(PCD8544_SCK_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_SCK_PAD);
  gpio_set_af(PCD8544_SCK_PORT, PCD8544_SCK_AF, PCD8544_SCK_PAD);

  gpio_mode_setup(PCD8544_CS_PORT, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, PCD8544_CS_PAD);
  gpio_set_output_options(PCD8544_CS_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_CS_PAD);
#endif
  gpio_set(PCD8544_CS_PORT, PCD8544_CS_PAD);

  /* data/command select line, reset line */
#ifdef STM32F1
  gpio_set_mode(PCD8544_DC_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_DC_PAD);
  gpio_set_mode(PCD8544_RES_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_RES_PAD);
#else
  gpio_mode_setup(PCD8544_DC_PORT, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, PCD8544_DC_PAD);
  gpio_set_output_options(PCD8544_DC_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_DC_PAD);

  gpio_mode_setup(PCD8544_RES_PORT, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, PCD8544_RES_PAD);
  gpio_set_output_options(PCD8544_RES_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_RES_PAD);
#endif
  
  /* reset on */
  gpio_clear(PCD8544_RES_PORT, PCD8544_RES_PAD);

#ifdef PCD8544_BL_PWM
  /* backlight pwm output */
#ifdef STM32F1
  gpio_set_mode(PCD8544_BL_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PCD8544_BL_PAD);
#else
  gpio_mode_setup(PCD8544_BL_PORT, GPIO_MODE_AF,
                  GPIO_PUPD_NONE, PCD8544_BL_PAD);
  gpio_set_output_options(PCD8544_BL_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_BL_PAD);
  gpio_set_af(PCD8544_BL_PORT, PCD8544_BL_AF, PCD8544_BL_PAD);
#endif

  /* Disable output */
  timer_disable_oc_output(PCD8544_BL_DEV, PCD8544_BL_CH);
  
  /* Configure global mode of line */
  timer_disable_oc_clear(PCD8544_BL_DEV, PCD8544_BL_CH);
  timer_enable_oc_preload(PCD8544_BL_DEV, PCD8544_BL_CH);
  timer_set_oc_slow_mode(PCD8544_BL_DEV, PCD8544_BL_CH);
  timer_set_oc_mode(PCD8544_BL_DEV, PCD8544_BL_CH, TIM_OCM_PWM1);
  
  /* Configure OC */
  timer_set_oc_polarity_high(PCD8544_BL_DEV, PCD8544_BL_CH);
  timer_set_oc_idle_state_unset(PCD8544_BL_DEV, PCD8544_BL_CH);
  
  /* Set the output compare value for OC */
  timer_set_oc_value(PCD8544_BL_DEV, PCD8544_BL_CH, 0);
  
  /* Enable output */
  timer_enable_oc_output(PCD8544_BL_DEV, PCD8544_BL_CH);
#endif

#ifdef PCD8544_BL_GPO
  /* backlight gpio on/off */
#ifdef STM32F1
  gpio_set_mode(PCD8544_BL_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_BL_PAD);
#else
  gpio_mode_setup(PCD8544_BL_PORT, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, PCD8544_BL_PAD);
  gpio_set_output_options(PCD8544_BL_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH,
                          PCD8544_BL_PAD);
#endif
#endif

#ifndef STM32F1
#define SPI_CR1_DFF_8BIT SPI_CR1_CRCL_8BIT
#endif

  /* configure SPI */
  spi_reset(PCD8544_SPI_DEV);
  spi_init_master(PCD8544_SPI_DEV, PCD8544_SPI_DIV,
                  SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE,
                  SPI_CR1_CPHA_CLK_TRANSITION_1,
                  SPI_CR1_DFF_8BIT,
                  SPI_CR1_MSBFIRST);
  spi_set_bidirectional_transmit_only_mode(PCD8544_SPI_DEV);
  spi_enable_software_slave_management(PCD8544_SPI_DEV);
  spi_set_nss_high(PCD8544_SPI_DEV);

#ifdef PCD8544_DMA
  /* configure DMA channel */
  dma_channel_reset(PCD8544_DMA_DEV, PCD8544_DMA_CH);
  dma_set_peripheral_address(PCD8544_DMA_DEV, PCD8544_DMA_CH, (uint32_t)&SPI_DR(PCD8544_SPI_DEV));
  dma_set_memory_address(PCD8544_DMA_DEV, PCD8544_DMA_CH, (uint32_t)pcd8544_frame_buffer);
  dma_set_number_of_data(PCD8544_DMA_DEV, PCD8544_DMA_CH, sizeof(pcd8544_frame_buffer));
  dma_set_priority(PCD8544_DMA_DEV, PCD8544_DMA_CH, DMA_CCR_PL_LOW);
  dma_set_read_from_memory(PCD8544_DMA_DEV, PCD8544_DMA_CH);
  dma_enable_memory_increment_mode(PCD8544_DMA_DEV, PCD8544_DMA_CH);
  dma_set_peripheral_size(PCD8544_DMA_DEV, PCD8544_DMA_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(PCD8544_DMA_DEV, PCD8544_DMA_CH, DMA_CCR_MSIZE_8BIT);

  nvic_set_priority(PCD8544_DMA_IRQ, 0);
	nvic_enable_irq(PCD8544_DMA_IRQ);
  dma_enable_transfer_complete_interrupt(PCD8544_DMA_DEV, PCD8544_DMA_CH);
#endif
  
  /* reset off */
  gpio_set(PCD8544_RES_PORT, PCD8544_RES_PAD);

  pcd8544_set_command();
  pcd8544_spi_enable();

  pcd8544_command(PCD8544_FUNCTION_SET(PCD8544_POWER_ON, PCD8544_HORIZONTAL, PCD8544_EXTENDED));
  pcd8544_command(PCD8544_BIAS_SYSTEM(PCD8544_BIAS_N4_1S48));
  pcd8544_command(PCD8544_OPERATION_VOLTAGE(64));
  pcd8544_command(PCD8544_TEMPERATURE_CONTROL(0));

  pcd8544_command(PCD8544_FUNCTION_SET(PCD8544_POWER_ON, PCD8544_HORIZONTAL, PCD8544_BASIC));
  pcd8544_command(PCD8544_DISPLAY_CONTROL(PCD8544_NORMAL));
  pcd8544_command(PCD8544_SET_X_ADDRESS(0));
  pcd8544_command(PCD8544_SET_Y_ADDRESS(0));

  pcd8544_spi_disable();
  pcd8544_set_data();
}

void pcd8544_backlight(uint16_t level) {
#ifdef PCD8544_BL_PWM
  timer_set_oc_value(PCD8544_BL_DEV, PCD8544_BL_CH,
                     (uint32_t)level * ((uint32_t)TIM_ARR(PCD8544_BL_DEV) + 1) / ((uint32_t)(uint16_t)100));
                     
#endif
#ifdef PCD8544_BL_GPO
  if ((level > 0) == !!PCD8544_BL_ON)
    gpio_set(PCD8544_BL_PORT, PCD8544_BL_PAD);
  else
    gpio_clear(PCD8544_BL_PORT, PCD8544_BL_PAD);
#endif
}

static inline uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

bool pcd8544_active(void) {
#ifdef PCD8544_DMA
  return dma_is_active(PCD8544_DMA_DEV, PCD8544_DMA_CH);
#else
  return false;
#endif
}

void pcd8544_update(void) {
#ifdef PCD8544_DMA
  if (dma_is_active(PCD8544_DMA_DEV, PCD8544_DMA_CH)) {
    return;
  }
#endif
  
  pcd8544_spi_enable();

#ifdef PCD8544_DMA
  dma_set_number_of_data(PCD8544_DMA_DEV, PCD8544_DMA_CH, sizeof(pcd8544_frame_buffer));
  dma_enable_channel(PCD8544_DMA_DEV, PCD8544_DMA_CH);
  spi_enable_tx_dma(PCD8544_SPI_DEV);
#endif

#ifndef PCD8544_DMA
  
#endif
}

#ifdef PCD8544_DMA
void pcd8544_dma_isr(void) {
  dma_clear_interrupt_flags(PCD8544_DMA_DEV, PCD8544_DMA_CH, DMA_GIF | DMA_TCIF | DMA_HTIF);

  dma_disable_channel(PCD8544_DMA_DEV, PCD8544_DMA_CH);
  spi_disable_tx_dma(PCD8544_SPI_DEV);

  pcd8544_spi_disable();
}
#endif

void pcd8544_done(void) {
  pcd8544_spi_enable();
  
  pcd8544_set_command();
  
  pcd8544_command(PCD8544_FUNCTION_SET(PCD8544_POWER_OFF, PCD8544_HORIZONTAL, PCD8544_BASIC));

  /* de-configure SPI */
  pcd8544_spi_disable();
  
#ifdef PCD8544_BL_PWM
  /* Disable output */
  timer_disable_oc_output(PCD8544_BL_DEV, PCD8544_BL_CH);
#endif

#ifdef PCD8544_DMA
  dma_disable_transfer_complete_interrupt(PCD8544_DMA_DEV, PCD8544_DMA_CH);
  
  nvic_disable_irq(PCD8544_DMA_IRQ);
#endif

#ifdef STM32F1
  /* SPI bus outputs */
  gpio_set_mode(PCD8544_DI_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, PCD8544_DI_PAD);
  gpio_set_mode(PCD8544_SCK_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, PCD8544_SCK_PAD);
  gpio_set_mode(PCD8544_CS_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, PCD8544_CS_PAD);

  /* data/command select line, reset line */
  gpio_set_mode(PCD8544_DC_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, PCD8544_DC_PAD);
  gpio_set_mode(PCD8544_RES_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, PCD8544_RES_PAD);

  /* backlight pwm output */
  gpio_set_mode(PCD8544_BL_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, PCD8544_BL_PAD);
#else
  
#endif

  rcc_periph_clock_disable(PCD8544_SPI_RCC);
}
