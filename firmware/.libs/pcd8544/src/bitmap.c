#include "bitmap.h"

void bitmap_init(const bitmap_canvas_t *const c) {
#if BITMAP_USE_AFFINE
  bitmap_set_transform(c, &bitmap_identity);

  if (c->bound.b.x < c->bound.a.x) { /* flip x */
    
  }
  
  if (c->bound.b.y < c->bound.a.y) { /* flip y */
    
  }
#endif
  
  c->state->cols = c->bound.b.x > c->bound.a.x ? c->bound.b.x - c->bound.a.x : c->bound.a.x - c->bound.b.x;
  c->state->rows = c->bound.b.y > c->bound.a.y ? c->bound.b.y - c->bound.a.y : c->bound.a.y - c->bound.b.y;
}

#if BITMAP_USE_AFFINE
const bitmap_matrix_t bitmap_identity = {{ 1, 0, 0, 1, 0, 0 }};

/*
 * a c e 
 * b d f
 * 0 0 1
 */

static void bitmap_matrix_mul(bitmap_matrix_t *r, const bitmap_matrix_t *const m, const bitmap_matrix_t *const n) {
  r->a = m->a * n->a + m->c * n->b;
  r->b = m->b * n->a + m->d * n->b;
  r->c = m->a * n->c + m->c * n->d;
  r->d = m->b * n->c + m->d * n->d;
  r->e = m->a * n->e + m->c * n->f + m->e;
  r->f = m->b * n->e + m->d * n->f + m->f;
}

void bitmap_transform(const bitmap_canvas_t *const c, const bitmap_matrix_t *const m) {
  bitmap_matrix_t t = c->state->trans;
  bitmap_matrix_mul(&c->state->trans, &t, m);
}
#endif

void bitmap_apply(const bitmap_canvas_t *const c) {
  bitmap_cell_t *b = c->frame, *e = c->frame + c->state->rows / 8 * c->state->cols;

  switch (c->state->flags & bitmap_operation_mask) {
  case bitmap_draw:
    for (; b < e; *b++ = 0xff);
    break;
  case bitmap_clear:
    for (; b < e; *b++ = 0x00);
    break;
  case bitmap_invert:
    for (; b < e; *b++ ^= 0xff);
    break;
  }
}

static void _bitmap_fix_coords(const bitmap_canvas_t *const c,
                               bitmap_coord_t *x,
                               bitmap_coord_t *y) {
  if (x && c->bound.b.x < c->bound.a.x) { /* flip x */
    *x = c->state->cols - *x - 1;
  }
  
  if (y && c->bound.b.y < c->bound.a.y) { /* flip y */
    *y = c->state->rows - *y - 1;
  }
}

static void _bitmap_pixel(const bitmap_canvas_t *const c,
                          bitmap_coord_t x, bitmap_coord_t y) {
  _bitmap_fix_coords(c, &x, &y);
  
  bitmap_cell_t *b = &c->frame[(y / 8) * c->state->cols + x];
  bitmap_cell_t v = 1 << (y % 8);
  
  switch (c->state->flags & bitmap_operation_mask) {
  case bitmap_draw:
    *b |= v;
    break;
  case bitmap_clear:
    *b &= ~v;
    break;
  case bitmap_invert:
    *b ^= v;
    break;
  }
}

#define _swap2(a, b) { typeof(a) _ = a; a = b; b = _; }

static void _bitmap_point(const bitmap_canvas_t *const c,
                          bitmap_coord_t x, bitmap_coord_t y) {
#if BITMAP_USE_CHECK
  if (x < 0 || x >= c->state->cols ||
      y < 0 || y >= c->state->rows)
    return;
#endif  
  _bitmap_pixel(c, x, y);
}

static void _bitmap_hline(const bitmap_canvas_t *const c,
                          bitmap_coord_t x, bitmap_coord_t y,
                          bitmap_coord_t X) {
  _bitmap_fix_coords(c, &x, &y);
  _bitmap_fix_coords(c, &X, NULL);
  
  if (y < 0 || y >= c->state->rows)
    return;

  if (x > X)
    _swap2(x, X);
  
  if (x < 0)
    x = 0;

  if (x >= c->state->cols)
    x = c->state->cols - 1;

  bitmap_cell_t v = 1 << (y % 8);
  bitmap_cell_t *b = &c->frame[(y / 8) * c->state->cols], *e;
  e = b + X;
  b += x;
  
  switch (c->state->flags & bitmap_operation_mask) {
  case bitmap_draw:
    for (; b <= e; *b++ |= v);
    break;
  case bitmap_clear:
    for (; b <= e; *b++ &= ~v);
    break;
  case bitmap_invert:
    for (; b <= e; *b++ ^= v);
    break;
  }
}

static void _bitmap_vline(const bitmap_canvas_t *const c,
                          bitmap_coord_t x, bitmap_coord_t y,
                          bitmap_coord_t Y) {
  _bitmap_fix_coords(c, &x, &y);
  _bitmap_fix_coords(c, NULL, &Y);

#if BITMAP_USE_CHECK
  if (x < 0 || x >= c->state->cols)
    return;
#endif
  
  if (y > Y)
    _swap2(y, Y);

#if BITMAP_USE_CHECK
  if (y < 0)
    y = 0;

  if (y >= c->state->rows)
    y = c->state->rows - 1;
#endif
  bitmap_cell_t *b = &c->frame[(y / 8) * c->state->cols + x],
    *e = &c->frame[(Y / 8) * c->state->cols + x];
  bitmap_cell_t i = 0, iy = y % 8, iY = Y % 8, v = 0, V = 0;

  for (; i < 8; i++) {
    if (i >= iy)
      v |= 1 << i;
    if (i <= iY)
      V |= 1 << i;
  }
  
  switch (c->state->flags & bitmap_operation_mask) {
  case bitmap_draw:
    if (e == b) {
      *b |= v & V;
      break;
    }
    *b |= v;
    for (b += c->state->cols; b < e; b += c->state->cols)
      *b |= 0xff;
    *b |= V;
    break;
  case bitmap_clear:
    if (e == b) {
      *b &= ~(v & V);
      break;
    }
    *b &= ~v;
    for (b += c->state->cols; b < e; b += c->state->cols)
      //*b &= ~0xff;
      *b &= 0x00;
    *b &= ~V;
    break;
  case bitmap_invert:
    if (e == b) {
      *b ^= v & V;
      break;
    }
    *b ^= v;
    for (b += c->state->cols; b < e; b += c->state->cols)
      *b ^= 0xff;
    *b ^= V;
    break;
  }
}

void bitmap_rect(const bitmap_canvas_t *const c,
                 bitmap_coord_t x, bitmap_coord_t y,
                 bitmap_coord_t X, bitmap_coord_t Y) {
  if (x > X)
    _swap2(x, X);

  if (y > Y)
    _swap2(y, Y);
  
  if (c->state->flags & bitmap_line) {
#if 0
    bitmap_point_t p = {{ x, y }};
    
    for (; p.x < X; p.x++)
      _bitmap_point(c, f, p.x, p.y);
    for (; p.y < Y; p.y++)
      _bitmap_point(c, f, p.x, p.y);
    for (; p.x > x; p.x--)
      _bitmap_point(c, f, p.x, p.y);
    for (; p.y > y; p.y--)
      _bitmap_point(c, f, p.x, p.y);
#else
    _bitmap_hline(c, x, y, X - 1);
    _bitmap_vline(c, X, y, Y - 1);
    _bitmap_hline(c, x + 1, Y, X);
    _bitmap_vline(c, x, y + 1, Y);
#endif
  }

  if (c->state->flags & bitmap_fill) {
#if 0
    bitmap_point_t p;
    
    for (p.x = x + 1; p.x < X; p.x++)
      for (p.y = y + 1; p.y < Y; p.y++)
        _bitmap_point(c, p.x, p.y);
#else
    for (y++; y < Y; y++)
      _bitmap_hline(c, x + 1, y, X - 1);
#endif
  }
}

void bitmap_circle(const bitmap_canvas_t *const c,
                   bitmap_coord_t X, bitmap_coord_t Y,
                   bitmap_coord_t R) {  
  bitmap_coord_t x = 0, y = R, delta = 1 - R * 2, error = 0;
  
  for (; y >= 0; ) {
    _bitmap_point(c, X + x, Y + y);
    _bitmap_point(c, X + x, Y - y);
    _bitmap_point(c, X - x, Y + y);
    _bitmap_point(c, X - x, Y - y);
    
    error = (delta + y) * 2 - 1;
    
    if (delta < 0 && error <= 0) {
      delta += ++x * 2 + 1;
      continue;
    }
    
    error = (delta - x) * 2 - 1;
    
    if (delta > 0 && error > 0) {
      delta += 1 - --y * 2;
      continue;
    }
    
    delta += 2 * (++x - y--);
  }
}

static void _bitmap_line(const bitmap_canvas_t *const c,
                         const bitmap_point_t *const a,
                         const bitmap_point_t *const b) {
  bitmap_point_t s, d, p;
  bitmap_coord_t i = 0, I, e2 = 0, de2, dc2;

  d.x = b->x - a->x;
  d.y = b->y - a->y;

  if (d.x < 0) {
    d.x = -d.x;
    s.x = -1;
  } else {
    s.x = 1;
  }

  if (d.y < 0) {
    d.y = -d.y;
    s.y = -1;
  } else {
    s.y = 1;
  }
  
  p = *a;

  if (d.x > d.y) {
    I = d.x;
    de2 = (bitmap_coord_t)d.y << 1;
    dc2 = (bitmap_coord_t)d.x << 1;
    
    for (; i <= I; i++) {
      _bitmap_point(c, p.x, p.y);
      p.x += s.x;
      e2 += de2;
      if (e2 >= d.x) {
        p.y += s.y;
        e2 -= dc2;
      }
    }
  } else {
    I = d.y;
    de2 = (bitmap_coord_t)d.x << 1;
    dc2 = (bitmap_coord_t)d.y << 1;

    for (; i <= I; i++) {
      _bitmap_point(c, p.x, p.y);
      p.y += s.y;
      e2 += de2;
      if (e2 >= d.y) {
        p.x += s.x;
        e2 -= dc2;
      }
    }
  }
}

void bitmap_polygon(const bitmap_canvas_t *c,
                    const bitmap_point_t *p,
                    bitmap_size_t l) {
  const bitmap_point_t *const e = p + l - 1;

  if (c->state->flags & bitmap_line) {
    for (; p < e; p++)
      _bitmap_line(c, p, p + 1);
    /* close
    if (l > 2)
      _bitmap_line(c, p, p - l + 1);
    */
  }

  if (c->state->flags & bitmap_fill) {
    /* TODO */
  }
}

#define PSF1_MAGIC0     0x36
#define PSF1_MAGIC1     0x04

#define PSF1_MODE512    0x01
#define PSF1_MODEHASTAB 0x02
#define PSF1_MODEHASSEQ 0x04
#define PSF1_MAXMODE    0x05

#define PSF1_SEPARATOR  0xFFFF
#define PSF1_STARTSEQ   0xFFFE

struct psf1_header {
  unsigned char magic[2];     /* Magic number */
  unsigned char mode;         /* PSF font mode */
  unsigned char charsize;     /* Character size */
};

static uint32_t _utf8_length(const char *text) {
  uint8_t i;
  uint32_t chars = 0;
  
  for (; *text != '\0'; chars++) {
    if ((*text & 0xc0) == 0xc0) {
      for (i = 5; i > 0 && *text & (1 << i); i--);
      text += 7 - i;
    } else {
      text++;
    }
  }
  
  return chars;
}

static uint32_t _utf8_decode(const char **text) {
  if ((**text & 0xc0) == 0xc0) {
    uint8_t i;
    uint32_t unicode_char = **text & ~0xc0;
    
    for (i = 5; i > 0 && (**text) & (1 << i); i--)
      unicode_char &= ~(1 << i);
    unicode_char &= ~(1 << i);
    
    const char *out = (*text)++ + 7 - i;
    for (; *text < out; ) {
      unicode_char <<= 6;
      unicode_char |= *(*text)++ & 0x3f;
    }
    return unicode_char;
  } else {
    return *(*text)++;
  }
}

#define _unicode_char16(ptr) ((((uint16_t)ptr[1]) << 8) | ((uint16_t)ptr[0]))

static const uint8_t *_unicode_lookup(const uint8_t *unicode_ptr,
                                      const uint8_t *unicode_end,
                                      uint32_t target_char,
                                      uint16_t *glyph_index) {
  for (*glyph_index = 0; unicode_ptr < unicode_end; ) {
    volatile uint16_t unicode_char = _unicode_char16(unicode_ptr);
    switch (unicode_char) {
    case PSF1_SEPARATOR:
      unicode_ptr += 2;
      (*glyph_index)++;
      break;
    case PSF1_STARTSEQ:
      /* skip sequence */
      for (unicode_ptr += 2; unicode_ptr < unicode_end &&
             _unicode_char16(unicode_ptr) != PSF1_SEPARATOR;
           unicode_ptr += 2);
      break;
    default:
      if (unicode_char == target_char) {
        /* seek to separator or sequence */
        for (unicode_ptr += 2; unicode_ptr < unicode_end &&
               (unicode_char = _unicode_char16(unicode_ptr)) != PSF1_SEPARATOR &&
               unicode_char != PSF1_STARTSEQ;
             unicode_ptr += 2);
        switch (unicode_char) {
        case PSF1_SEPARATOR:
        case PSF1_STARTSEQ:
          return unicode_ptr;
        }
      } else {
        unicode_ptr += 2;
      }
    }
  }
  return NULL;
}

#if 0
/* TODO: render sequences */
static const uint8_t *_unicode_sequence(const uint8_t *unicode_ptr,
                                        const uint8_t *unicode_end,
                                        const uint8_t *sequence_ptr,
                                        uint32_t *unicode_char) {
  for (; ; sequence_ptr += 2) {
    *unicode_char = _unicode_char16(sequence_ptr);
    switch (*unicode_char) {
    case PSF1_SEPARATOR:
      return NULL;
    case PSF1_STARTSEQ:
      break;
    default:
      return sequence_ptr;
    }
  }
}
#endif

static void _bitmap_glyph_render(const bitmap_canvas_t *const c,
                                 bitmap_coord_t x,
                                 bitmap_coord_t y,
                                 const bitmap_cell_t *glyph,
                                 bitmap_size_t rows) {
  uint8_t i, _i = 0;
  bitmap_coord_t X = x + 8, _x;
  bitmap_coord_t Y = y + rows;

  if (X < 0 ||
      Y < 0 ||
      x >= (bitmap_coord_t)c->state->cols ||
      y >= (bitmap_coord_t)c->state->rows)
    return;
  
  if (x < 0) {
    _i = -x;
    x = 0;
  }

  if (X >= c->state->cols) {
    X = c->state->cols - 1;
  }
  
  if (y < 0) {
    glyph += -y;
    y = 0;
  }

  if (Y >= c->state->rows) {
    Y = c->state->rows - 1;
  }
  
  for (; y < Y; y++, glyph++) {
    for (i = _i, _x = x; _x < X; _x++) {
      if ((*glyph) & (0x80 >> (i++))) {
        _bitmap_pixel(c, _x, y);
      }
    }
  }
}

void bitmap_text(const bitmap_canvas_t *const c,
                 bitmap_coord_t x,
                 bitmap_coord_t y,
                 const char *text) {
  const struct psf1_header *font_header = (const void*)c->state->font.data;
  const uint8_t *bitmap_data = (const uint8_t*)font_header + sizeof(struct psf1_header);
  const uint8_t *unicode_table = bitmap_data + font_header->charsize * (font_header->mode & PSF1_MODE512 ? 512 : 256);

  bitmap_size_t length = _utf8_length(text);
  
  if (c->state->flags & bitmap_left &&
      c->state->flags & bitmap_right) {
    x -= length * 8 / 2;
  } else if(c->state->flags & bitmap_right) {
    x -= length * 8;
  }
  
  if (c->state->flags & bitmap_top &&
      c->state->flags & bitmap_bottom) {
    y -= font_header->charsize / 2;
  } else if(c->state->flags & bitmap_bottom) {
    y -= font_header->charsize;
  }

  if (font_header->mode & PSF1_MODEHASTAB) {
    for (; *text != '\0'; x += 8) {
      uint32_t unicode_char = _utf8_decode(&text);

      uint16_t glyph_index;
      const uint8_t *sequence_ptr = _unicode_lookup(unicode_table,
                                                    c->state->font.data + c->state->font.size,
                                                    unicode_char,
                                                    &glyph_index);
      if (sequence_ptr != NULL) {
        if (font_header->mode & PSF1_MODEHASSEQ) {
          /* TODO */
        } else {
          _bitmap_glyph_render(c, x, y, &bitmap_data[glyph_index * font_header->charsize], font_header->charsize);
        }
      }
    }
  } else { /* no lookup table */
    for (; *text != '\0'; x += 8) {
      uint32_t unicode_char = _utf8_decode(&text);
      
      _bitmap_glyph_render(c, x, y, &bitmap_data[unicode_char * font_header->charsize], font_header->charsize);
    }
  }
}
