libpcd8544.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libpcd8544
libpcd8544.INHERIT := firmware libopencm3
libpcd8544.CDIRS := $(libpcd8544.BASEPATH)include
libpcd8544.SRCS := $(addprefix $(libpcd8544.BASEPATH)src/,\
  pcd8544.c \
  bitmap.c)
