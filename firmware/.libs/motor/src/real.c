#include <stdint.h>

#include "real.h"
#include "lookup.h"

lookup_table_defimpl(conv_real, real_t, real_t, uint8_t);

#define TRIG_LOOKUP_STEP ((M_PI / 2 + M_PI / 10) / TRIG_LOOKUP_SIZE)

#define trig_sin(a) real_make(sinf(a))

#include lookup_table_file(trig)
#define LOOKUP_FUNCTION(s) trig_sin((s) * TRIG_LOOKUP_STEP)
lookup_table_inst(conv_real, sin_table, trig, REAL_ZERO, real_make(TRIG_LOOKUP_STEP));
#undef LOOKUP_FUNCTION

real_t _real_sin(real_t a) {
  char n = 0;

  for (; a > REAL_TWO_PI; ) {
    a = real_sub(a, REAL_TWO_PI);
  }
  
  for (; a < REAL_ZERO; ) {
    a = real_add(a, REAL_TWO_PI);
  }
  
  if (a > REAL_ONE_PI) {
    n = 1;
    a = real_sub(a, REAL_ONE_PI);
  }
  
  if (a > REAL_HALF_PI) {
    a = real_sub(REAL_ONE_PI, a);
  }
  
  real_t r = conv_real_lookup(&sin_table, a);
  
  return n ? -r : r;
}

real_t _real_cos(real_t a) {
  return _real_sin(real_add(a, REAL_HALF_PI));
}
