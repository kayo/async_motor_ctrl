#include "swm.h"

void swm_init(swm_t *s) {
	s->angle = REAL_ZERO;
}

void swm_step(const mf_t *i, swm_t *s, pwm3_t *o) {
	/* Ta = 0.5 + 0.5 * sin(a) */
  o->Ta = real_add(REAL_HALF, real_mul(REAL_HALF, real_sin(s->angle)));
	/* Tb = 0.5 + 0.5 * sin(a + PI * 2 / 3) */
  o->Tb = real_add(REAL_HALF, real_mul(REAL_HALF, real_sin(real_add(s->angle, REAL_TWO_THIRD_PI))));
	/* Tc = 0.5 + 0.5 * sin(a - PI * 2 / 3) */
  o->Tc = real_add(REAL_HALF, real_mul(REAL_HALF, real_sin(real_sub(s->angle, REAL_TWO_THIRD_PI))));
	
	/* a = a + s */
	s->angle = real_add(s->angle, i->step);
  
	if (real_ge(s->angle, REAL_TWO_PI)) {
		/* a = a - 2 * PI */
		s->angle = real_sub(s->angle, REAL_TWO_PI);
	}
}
