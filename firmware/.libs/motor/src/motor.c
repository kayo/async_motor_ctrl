#include "motor.h"

void mf_init(mf_t *i, real_t f) {
	i->period = real_div(REAL_TWO_PI, f);
}

void mf_set(mf_t *i, real_t f) {
	i->step = real_mul(f, i->period);
}

void pwm3_init(pwm3_t *s) {
  s->Ta = s->Tb = s->Tc = REAL_ZERO;
}
