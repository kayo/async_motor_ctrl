#ifndef __REAL_H__
#define __REAL_H__
/**
 * @defgroup real Generic real-number layer
 * @brief This part implements the middle layer of the real-number arithmetic operations
 *
 * This layer can be configured to use some backends:
 *
 * @li float - Default platform/compiler specific floating point arithmetic
 * @li fixed - Internal implementation of Q15.16 fixed-point arithmetic including lookup-table based trigonometry functions.
 *
 * Platform-dependent backends:
 *
 * @li qfplib - Excellent QFP floating-point library for ARM Cortex-M0/M3 cores.
 * @li stdfix - Standard (?) C99 fixed-point extension.
 *
 * @{
 */

#include "macro.h"

#define REAL_float 0
#define REAL_fixed 1
#define REAL_qfplib 2
#define REAL_stdfix 3

#define IS_REAL_LIB(x) (_CAT2(REAL_, x)==_CAT2(REAL_, REAL_ENGINE))

#include <math.h>

#if IS_REAL_LIB(fixed)
#include "fixed.h"

typedef fixed real_t;

#define real_make(a) fp_make(a)
#define real_float(a) fp_float(a)
#define real_add(a, b) fp_add(a, b)
#define real_sub(a, b) fp_sub(a, b)
#define real_mul(a, b) fp_mul(a, b)
#define real_div(a, b) fp_div(a, b)
//#define real_sin(a) fp_sin(a)
#define real_sin(a) _real_sin(a)
#define real_cos(a) _real_cos(a)
#endif /* IS_REAL_LIB(fixed) */

#if IS_REAL_LIB(qfplib)
#include "qfplib.h"

typedef float real_t;

#define real_make(a) ((real_t)(a))
#define real_float(a) ((float)(a))
#define real_add(a, b) qfp_fadd(a, b)
#define real_sub(a, b) qfp_fsub(a, b)
#define real_mul(a, b) qfp_fmul(a, b)
#define real_div(a, b) qfp_fdiv(a, b)
#define real_sin(a) qfp_fsin(a)
#define real_cos(a) qfp_fcos(a)
#endif /* IS_REAL_LIB(qfplib) */

#if IS_REAL_LIB(stdfix)
#include <stdfix.h>

typedef accum real_t;

#define real_make(a) ((real_t)(a))
#define real_float(a) ((float)(a))
#define real_add(a, b) ((real_t)(a)+(real_t)(b))
#define real_sub(a, b) ((real_t)(a)-(real_t)(b))
#define real_mul(a, b) ((real_t)(a)*(real_t)(b))
#define real_div(a, b) ((real_t)(a)/(real_t)(b))
#define real_sin(a) _real_sin(a)
#define real_cos(a) _real_cos(a)
#endif /* IS_REAL_LIB(stdfix) */

#if IS_REAL_LIB(float)
typedef float real_t;

#define real_make(a) ((real_t)(a))
#define real_float(a) ((float)(a))
#define real_add(a, b) ((real_t)(a)+(real_t)(b))
#define real_sub(a, b) ((real_t)(a)-(real_t)(b))
#define real_mul(a, b) ((real_t)(a)*(real_t)(b))
#define real_div(a, b) ((real_t)(a)/(real_t)(b))
#define real_sin(a) sinf(a)
#define real_cos(a) cosf(a)
#endif /* IS_REAL_LIB(float) */

#define real_lt(a, b) ((a) < (b))
#define real_le(a, b) ((a) <= (b))
#define real_gt(a, b) ((a) > (b))
#define real_ge(a, b) ((a) >= (b))
#define real_neg(a) (-(a))
#define real_abs(a) (real_lt(a, REAL_ZERO) ? real_neg(a) : (a))

#define REAL_ZERO real_make(0.0)
#define REAL_ONE real_make(1.0)
#define REAL_TWO real_make(2.0)
#define REAL_HALF real_make(0.5)
#define REAL_SQRT_THREE real_make(sqrt(3.0))
#define REAL_INV_SQRT_THREE real_make(1.0/sqrt(3.0))

#define REAL_ONE_PI real_make(M_PI)
#define REAL_TWO_PI real_make(M_PI * 2)
#define REAL_HALF_PI real_make(M_PI / 2)
#define REAL_THIRD_PI real_make(M_PI / 3)
#define REAL_TWO_THIRD_PI real_make(M_PI * 2 / 3)

real_t _real_sin(real_t a);
real_t _real_cos(real_t a);

/**
 * @}
 */
#endif /* __REAL_H__ */
