#ifndef __CLARKE__
#define __CLARKE__
/**
 * @defgroup clarke Clarke transformations
 * @brief This module implements direct and inverted Clarke transformations
 *
 * @{
 */

#include "motor.h"

/**
 * @brief Direct Clarke transformation
 *
 * @param[in] ab The pointer to @p a-b (or @p a-b-c) vector values
 * @param[out] dq The pointer to @p d-q vector values
 */
void direct_clarke(const ab_t *ab, dq_t *dq);

/**
 * @brief Invert Clarke transformation
 *
 * @param[in] dq The pointer to @p d-q vector values
 * @param[out] abc The pointer to @p a-b-c vector values
 */
void invert_clarke(const dq_t *dq, abc_t *abc);

/**
 * @}
 */
#endif /* __CLARKE__ */
