#ifndef __PARK__
#define __PARK__
/**
 * @defgroup park Park transformations
 * @brief This module implements direct and inverted Park transformations
 *
 * @{
 */

#include "motor.h"

/**
 * @brief Direct Park transformation
 *
 * @param[in] dqs The pointer to static @p d-q vector values
 * @param[in] phy The rotating angle value
 * @param[out] dqr The pointer to rotating @p d-q vector values
 */
void direct_park(const dq_t *dqs, real_t phy, dq_t *dqr);

/**
 * @brief Invert Park transformation
 *
 * @param[in] dqr The pointer to rotating @p d-q vector values
 * @param[in] phy The rotating angle value
 * @param[out] dqs The pointer to static @p d-q vector values
 */
void invert_park(const dq_t *dqr, real_t phy, dq_t *dqs);

/**
 * @}
 */
#endif /* __PARK__ */
