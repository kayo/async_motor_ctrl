#ifndef __ENC_H__
#define __ENC_H__ "enc.h"

#include "evt.h"

#define enc_fn(N, F) _CAT4(enc, N, _, F)

#define enc_def(N)            \
  void enc_fn(N, init)(void); \
  void enc_fn(N, done)(void)

#endif /* __ENC_H__ */
