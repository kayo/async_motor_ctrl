#include <libopencm3/cm3/dwt.h>

#include "config.h"
#include "delay.h"

void delay_init() {
  dwt_enable_cycle_counter();
}

delay_t delay_snap(void) {
  return dwt_read_cycle_counter();
}

void delay_wait(delay_t sc, delay_t dt) {
  delay_t nc = sc + dt;
  
  /* overflow */
  if (nc < sc) for (; dwt_read_cycle_counter() >= sc; );
  
  for (; dwt_read_cycle_counter() < nc; );
}

delay_t delay_meas(delay_t sc) {
  delay_t nc = dwt_read_cycle_counter();

  return sc <= nc ? nc - sc : ((1 << 24) - 1) - sc + nc;
}
