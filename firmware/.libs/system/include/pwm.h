#ifndef __PWM_H__
#define __PWM_H__ "pwm.h"

#define pwm_fn(N, F) _CAT4(pwm, N, _, F)

#define pwm_def(N)                        \
  void pwm_fn(N, init)(void);             \
  void pwm_fn(N, done)(void);             \
  void pwm_fn(N, set1)(uint16_t val);     \
  void pwm_fn(N, set2)(uint16_t val);     \
  void pwm_fn(N, set3)(uint16_t val);     \
  void pwm_fn(N, set4)(uint16_t val)

#endif /* __PWM_H__ */
