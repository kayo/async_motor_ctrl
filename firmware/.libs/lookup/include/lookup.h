#ifndef __LOOKUP_H__
#define __LOOKUP_H__ "lookup.h"

#define lookup_table_def(type, arg_t, res_t, idx_t) \
  typedef struct {                                  \
    arg_t from;                                     \
    arg_t step;                                     \
    idx_t size;                                     \
    res_t data[];                                   \
  } type##_t;                                       \
  res_t type##_lookup(const type##_t *tab, arg_t arg);

#define lookup_table_inst(type, name, data, from, step)                 \
  static const type##_t name = { from, step, LOOKUP_TABLE_##data##_DATA }

#define lookup_table(type, name, from, step) \
  lookup_table_inst(type, name, name, from, step)

#define lookup_table_impl(type, arg_t, res_t, idx_t)                    \
  res_t type##_lookup(const type##_t *tab, arg_t arg) {                 \
    arg_t a = arg - tab->from;                                          \
    idx_t i = a / tab->step;                                            \
    a -= tab->step * i;                                                 \
    if (i >= tab->size) i = tab->size - 1;                              \
    return tab->data[i + 1] > tab->data[i] ?                            \
      tab->data[i] + a * (tab->data[i + 1] - tab->data[i]) / tab->step : \
      tab->data[i] - a * (tab->data[i] - tab->data[i + 1]) / tab->step; \
  }

#define lookup_table_defimpl(type, arg_t, res_t, idx_t) \
  lookup_table_def(type, arg_t, res_t, idx_t);          \
  lookup_table_impl(type, arg_t, res_t, idx_t)

#define lookup_table_file(name) LOOKUP_TABLE_##name##_H

#endif /* __LOOKUP_H__ */
