#include "macro.h"
#include "config.h"

#include "pwrmon-def.h"
#include "sensor.h"
#include "driver.h"

extern uint16_t adc_val;

#define sensor_test(channel, value) adc_val = value; sensor_get(channel)

int main(void) {
  sensor_test(channel_voltage, 100);
  sensor_test(channel_voltage, 200);
  sensor_test(channel_voltage, 500);
  sensor_test(channel_voltage, 750);
  sensor_test(channel_voltage, 1024);
  
  driver_set(channel_voltage, 150);
  driver_set(channel_voltage, 200);
  driver_set(channel_voltage, 250);
  driver_set(channel_voltage, 300);
  driver_set(channel_voltage, 350);
  //driver_set(channel_voltage, 408);

  sensor_test(channel_current, 100);
  sensor_test(channel_current, 200);
  sensor_test(channel_current, 500);
  sensor_test(channel_current, 750);
  sensor_test(channel_current, 1024);
  
  driver_set(channel_current, 10);
  driver_set(channel_current, 15);
  driver_set(channel_current, 20);
  driver_set(channel_current, 25);
  driver_set(channel_current, 30);
  driver_set(channel_current, 35);
  driver_set(channel_current, 40);
  //driver_set(channel_current, 110);
  
  return 0;
}
