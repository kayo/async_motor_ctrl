#ifndef _PWRMON_DEF_H_
#define _PWRMON_DEF_H_

enum {
  pwrmon_get_voltage = 0x1,
  pwrmon_get_current = 0x2,
  
  pwrmon_get_max_voltage = 0x3,
  pwrmon_get_max_current = 0x4,
  
  pwrmon_set_max_voltage = 0x5,
  pwrmon_set_max_current = 0x6,
};

#include <stdint.h>

typedef uint8_t pwrmon_packet_t[2];

static inline uint8_t pwrmon_get_field(const pwrmon_packet_t pkt) {
  return pkt[0] >> 4;
}

static inline uint16_t pwrmon_get_value(const pwrmon_packet_t pkt) {
  return ((uint16_t)(pkt[0] & 0xf) << 8) | (uint16_t)pkt[1];
}

static inline void pwrmon_set_field(pwrmon_packet_t pkt, uint8_t field) {
  pkt[0] = (pkt[0] & 0xf) | (field << 4);
}

static inline void pwrmon_set_value(pwrmon_packet_t pkt, uint16_t value) {
  pkt[0] = (pkt[0] & ~0xf) | (uint8_t)(value >> 8);
  pkt[1] = (uint8_t)value;
}

static inline void pwrmon_mk_packet(pwrmon_packet_t pkt, uint8_t field, uint16_t value) {
  pkt[0] = (uint8_t)(field << 4) | (uint8_t)(value >> 8);
  pkt[1] = (uint8_t)value;
}

#endif /* _PWRMON_DEF_H_ */
