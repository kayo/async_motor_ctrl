#ifndef _UART_H_

#include <stdint.h>

enum {
  uart_ok = 0,
  uart_idle,
  uart_error,
};

void uart_init(void);
void uart_send(const uint8_t *ptr, uint8_t len);
uint8_t uart_recv(uint8_t *ptr, uint8_t *len);

#endif/*_UART_H_*/
