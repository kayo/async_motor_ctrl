/* uart interface */
#define uart_config soft,9600 /* soft|hard, baudrate */
#define uart_tx_pad B,4 /* port, pad */
#define uart_rx_pad B,3 /* port, pad */

/* chip select */
#define cs_config pcint,3,0 /* intr type, intr pad, active level */
#define cs_input B,3 /* port, pad */

/* sensor filter: buffer length, median filter count */
//#define sensor_config 16,8
#define sensor_config 1,0

/* analog channels */
#define current_input B,5,0 /* port, pad, channel */
#define voltage_input B,2,1 /* port, pad, channel */

/* protection reference driver: timer */
#define driver_config 0

/* pwm channels: port,pad,timer_channel */
#define current_output B,0,A
#define voltage_output B,1,B
